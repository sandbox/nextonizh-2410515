Drupal.behaviors.vk_comments = function(context) {
 settings = Drupal.settings.vk_comments;
 if (settings.app_id){
   VK.init({apiId: settings.app_id, onlyWidgets: true});
   VK.Widgets.Comments("vk_comments", settings.options, settings.page_id);
 }
}